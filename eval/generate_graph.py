from matplotlib import pyplot as plt
import networkx as nx

from decentralizepy.graphs.Regular import Regular

file_name = "40_regular"
g = Regular(40, 6, 111)
g.connect_graph()
g.write_graph_to_file(file_name + ".edges")

g = nx.read_edgelist(file_name + ".edges")
nx.draw(g)
plt.savefig("local/" + file_name + ".png")