import logging
import random

from decentralizepy.sharing.PartialModel import PartialModel
from decentralizepy.utils import identity


class RandomWalkPartialSharing(PartialModel):
    """
    This class implements the partial model sharing with nodes communicating as Random Walk Model.
    """

    def __init__(
            self,
            rank,
            machine_id,
            communication,
            mapping,
            graph,
            model,
            dataset,
            log_dir,
            alpha=1.0,
            dict_ordered=True,
            save_shared=False,
            metadata_cap=1.0,
            save_accumulated="",
            accumulation=False,
            change_transformer=identity,
            accumulate_averaging_changes=False,
    ):
        super().__init__(
            rank,
            machine_id,
            communication,
            mapping,
            graph,
            model,
            dataset,
            log_dir,
            alpha,
            dict_ordered,
            save_shared,
            metadata_cap,
            accumulation,
            save_accumulated,
            change_transformer,
            accumulate_averaging_changes,
        )
        self.age = 0

    def received_from_one(self):
        """
        Check if one neighbor has sent the current iteration.

        Returns
        -------
        bool
            True if required data has been received, False otherwise

        """
        for d in self.peer_deques.values():
            if len(d) != 0:
                return True
        return False

    def step(self):
        """
        Perform a sharing step. Implements Random Walk Model - sends data to a neighbor chosen at random.

        """
        self.age += 1
        self._pre_step()
        data = self.serialized_model()
        my_uid = self.mapping.get_uid(self.rank, self.machine_id)
        all_neighbors = self.graph.neighbors(my_uid)
        random_neighbor = random.choice(list(all_neighbors))
        data["age"] = self.age
        data["iteration"] = self.communication_round
        self.communication.send(random_neighbor, data)

        logging.info("Waiting for messages from neighbors")
        while not self.received_from_one():
            sender, data = self.communication.receive()
            if self.communication.received_bye:
                return
            logging.debug("Received model from {}".format(sender))
            age = data["age"]
            iteration = data["iteration"]
            del data["age"]
            del data["iteration"]
            self.peer_deques[sender].append((age, iteration, data))
            logging.info(
                "Deserialized received model from {} of iteration {}".format(
                    sender, iteration
                )
            )

        logging.info("Starting model averaging after receiving from one neighbor")
        total = dict()
        for key, value in self.model.state_dict().items():
            total[key] = self.age * value

        age_total = self.age
        max_age = self.age
        for d in self.peer_deques.values():
            if not d:
                continue
            age, iteration, data = d.popleft()
            logging.debug(
                "Averaging model from neighbor {} of iteration {}".format(d, iteration)
            )
            max_age = max(age, max_age)
            age_total += int(age)
            data = self.deserialized_model(data)
            for key, value in data.items():
                if key in total:
                    total[key] += value * int(age)
                else:
                    total[key] = value * int(age)

        for key, value in total.items():
            total[key] = value / age_total

        self.model.load_state_dict(total)
        logging.info("Model averaging complete")

        self.age = max_age
        self.communication_round += 1
        self._post_step()
