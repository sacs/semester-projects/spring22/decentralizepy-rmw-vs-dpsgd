import logging
import random

from decentralizepy.sharing.Sharing import Sharing


class RandomWalkSharing(Sharing):

    def __init__(
            self, rank, machine_id, communication, mapping, graph, model, dataset, log_dir
    ):
        """
        Constructor

        Parameters
        ----------
        rank : int
            Local rank
        machine_id : int
            Global machine id
        communication : decentralizepy.communication.Communication
            Communication module used to send and receive messages
        mapping : decentralizepy.mappings.Mapping
            Mapping (rank, machine_id) -> uid
        graph : decentralizepy.graphs.Graph
            Graph reprensenting neighbors
        model : decentralizepy.models.Model
            Model to train
        dataset : decentralizepy.datasets.Dataset
            Dataset for sharing data. Not implemented yer! TODO
        log_dir : str
            Location to write shared_params (only writing for 2 procs per machine)

        """
        super().__init__(rank, machine_id, communication, mapping, graph, model, dataset, log_dir)
        self.age = 0

    def received_from_one(self):
        """
        Check if one neighbor has sent the current iteration

        Returns
        -------
        bool
            True if required data has been received, False otherwise

        """
        for d in self.peer_deques.values():
            if len(d) != 0:
                return True
        return False

    def step(self):
        """
        Perform a sharing step. Implements Random Walk Model - sends data to a neighbor chosen at random.

        """
        self.age += 1
        data = self.serialized_model()
        my_uid = self.mapping.get_uid(self.rank, self.machine_id)
        all_neighbors = self.graph.neighbors(my_uid)
        random_neighbor = random.choice(list(all_neighbors))
        data["age"] = self.age
        data["iteration"] = self.communication_round
        self.communication.send(random_neighbor, data)

        logging.info("Waiting for messages from neighbors")
        while not self.received_from_one():
            sender, data = self.communication.receive()
            if self.communication.received_bye:
                return
            logging.debug("Received model from {}".format(sender))
            age = data["age"]
            iteration = data["iteration"]
            del data["age"]
            del data["iteration"]
            self.peer_deques[sender].append((age, iteration, data))
            logging.info(
                "Deserialized received model from {} of iteration {}".format(
                    sender, iteration
                )
            )

        logging.info("Starting model averaging after receiving from one neighbor")
        total = dict()
        for key, value in self.model.state_dict().items():
            total[key] = self.age * value

        age_total = self.age
        max_age = self.age
        for d in self.peer_deques.values():
            if not d:
                continue
            age, iteration, data = d.popleft()
            logging.debug(
                "Averaging model from neighbor {} of iteration {}".format(d, iteration)
            )
            max_age = max(age, max_age)
            age_total += int(age)
            data = self.deserialized_model(data)
            for key, value in data.items():
                if key in total:
                    total[key] += value * int(age)
                else:
                    total[key] = value * int(age)

        for key, value in total.items():
            total[key] = value / age_total

        self.model.load_state_dict(total)
        logging.info("Model averaging complete")

        self.age = max_age
        self.communication_round += 1
