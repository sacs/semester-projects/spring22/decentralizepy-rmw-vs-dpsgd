\chapter{Evaluation}\label{evaluation}
This section manifests the evaluation of the decentralized system in various setups and the experimental methodology employed. It provides a set of experiments examining different aspects of model sharing and communication. In each experiment, we describe its setup, followed by the results and corresponding assessment.

\section{Methodology}
Here, the three used datasets are described, followed by metrics employed for experiments evaluation, and specification of the test environment for running the experiments.

\subsection{Datasets}\label{datasets}
The experiments are run using three distinct datasets: MovieLens, Femnist, and Celeba. They are all significantly different in size, with MovieLens being the smallest and Femnist the biggest. Also, the MovieLens dataset is used for recommendation models, while the other two represent image type input for classification models.

\paragraph{MovieLens Latest}
The MovieLens Latest~\cite{movielens-latest} dataset consists of collections of ratings and free-text tagging made by users on a movie recommendation service MovieLens. The users' ratings correspond to how much they appreciated a given movie. The goal of the recommendation model is to predict which rating a user would give for a particular movie. There are in total 100 836 ratings, where the rating scale is from 0.5 to 5.0, incrementally on every 0.5. There are 610 users giving ratings across 9724 movies, where each has rated at least 20 movies. Both users and movies are represented only by an id, without any user's personal information or additional information for the movie.

\paragraph{FEMNIST}
FEMNIST~\cite{leaf} is a federated extended MNIST dataset built by partitioning the data of handwritten digits and characters based on their writer. The dataset consists of 3 550 users
and 805 263 total samples, with an average of 226.83 samples per user. The written digit/character is centered to fit an image size of 28x28 pixels. The dataset is utilized in image classification models to answer correctly which digit or character is written by the user.

\paragraph{CelebA}
Another image classification dataset is CelebA~\cite{leaf}. It is a large-scale face attributes dataset of celebrities, each having at least five images. The size of each image is 84x84 pixels. There are 200 288 samples in total with 9 343 different users, with an average of 21.44 samples per user. This dataset is used as input to the classifier that tells whether a person in the image is smiling or not.

\subsection{Metrics}
In order to measure the performance and determine the trade-offs of an experiment done on our decentralized system, multiple evaluation metrics are used. The most important ones are model train and test errors, test accuracy, network traffic, and total processing time. For the MovieLens dataset, the primary evaluation metric is test loss, where we report RMSE (Root Mean Square Error). While for FEMNIST and CelebA that is test accuracy, measuring the percentage of correct predictions as it is most suitable for classification models.

\subsection{Test Environment}
To run our simulated experiments, we used machines labostrex127-130 (in the IC cluster at EPFL). All the servers have the same specifications with processor Intel Xeon E5-2630 v3 at 2.40GHz and 128GB of memory running Ubuntu 20.04.2 LTS kernel 5.4.0-72.

\section{Experiments}\label{experiments}
In the following experiments, we use varied datasets presented in Section \ref{datasets}, multiple sharing methods depicted in Section \ref{sharing}, and two communication techniques explained in Section \ref{comm}. After experimenting with different setups, we present here the final experiments that best describe observed behavior. The experiments are done on a 4-regular graph with 96 nodes, using four machines, each maintaining 24 processes. In Table \ref{table-exp} we present parameter configurations used in experiments. We depict the machine learning models used, their tuned hyper-parameters, and a few additional parameters related to sharing in the system. Some parameters are obtained by running grid-search in a centralized setting and choosing the ones giving the best performance in terms of loss (or accuracy). For MovieLens(1), FEMNIST, and CelebA configurations, the parameter \emph{full\_epochs} is set to \emph{False}, meaning that the rounds are done on mini-batches. In contrast, for MovieLens(2), the local rounds are completed by training on the whole dataset.

\begin{table}[!htbp]
\centering
 \begin{tabular}{||c | c c c c ||} 
 \hline
  & MovieLens (1) & MovieLens (2) & FEMNIST & CelebA\\ [0.2ex] 
 \hline\hline
    ML Model & MatrixFactorization & MatrixFactorization & CNN & CNN\\ 
    Optimizer & SGD & Adam & SGD & SGD\\
    Loss & MSELoss & MSELoss & CrossEntropyLoss & CrossEntropyLoss\\ 
    Learning Rate & 0.25 & 0.01 & 0.01 & 0.001\\
    Rounds & 9 & 5 & 47 & 8\\
    Batch Size & 8 & 8 & 16 & 8\\
 \hline
 \end{tabular}
 \caption[Exp1]{Parameter configurations used in experiments.}
 \label{table-exp}
\end{table}

Both FEMNIST and CelebA use a convolutional neural network with different numbers and orders of convolutional, pooling, and fully connected layers. The activation function for both is \emph{ReLU}, while the number of classes for FEMNIST and CelebA is 62 and 2, respectively. Due to the complexity of CNN for the FEMNIST dataset, the total number of model parameters is 1.6 million, making this by far the biggest model of the three. For the CelebA dataset, this number is 28 000. With both of the datasets, the training data is initially distributed to the nodes such that data samples are disjunct, and training is done on a local dataset. In contrast, evaluation is done on global test data for all the nodes in the system. For the matrix factorization model used with MovieLens Latest dataset, the embedding size for both users and movies is 20 yielding a model with 413 360 parameters, with user and movie biases included. The train and test data is set by splitting each user's rating samples to a $70:30$ ratio. Each node receives a subset of users and its corresponding test and train samples. Model training is done on a local test set, while evaluation is done only on local test samples, unlike global testing used in the previous two datasets. This evaluation resembles the actual recommender system that focuses on learning the traits of its user and making recommendations specific to them. If the testing is done globally, the performance of the decentralized system is poor for the MovieLens dataset.

\subsection{Full Model}
The first experiment represents the fundamental comparison of two communication techniques, D-PSGD and RWM. This comparison is made by sharing the entire model. Since the network graph is 4-regular, each node has exactly four neighbors, and correspondingly in D-PSGD algorithm, a node receives 4 neighboring models in each round, after which it conducts an update to its local model. When using RWM, a node receives one model from a neighbor. 
\\
   
% 500 iterations
Running the decentralized system with the MovieLens dataset is examined for both versions of local training. When a round of train step for a node assumes training on the whole dataset, sharing the model with only one neighbor and updating it by weighted averaging is worse than sharing with all neighbors and doing Metropolis-Hastings. The performance measured in terms of test loss is shown in Figure \ref{ex1-ml-e-test-loss} with an RWM loss of 0.7630 and D-PSGD loss of 0.6684 at their final iterations. The loss calculated for the training phase is 0.0817 for RWM, and the model starts slowly overfitting. On the other hand, test loss for D-PSGD is 0.0985, with a continuation of learning. When the experiment is evaluated on a different type of local training, i.e., a round representing training only on mini-batches (with parameters defined in Table \ref{table-exp} MovieLens(1), there are still signs of overfitting for the RWM algorithm. The test losses for both models are depicted in Figure \ref{ex1-ml-s-test_loss}, and they are equal to 0.9754 and 0.8908 for RWM and D-PSGD, respectively, at their last iterations. Again, the performance of D-PSGD is better for completed iterations, but here the difference is almost negligible. In this case of local training, both communication versions produce a better model in the long run, as the learning is slow but constant. This behavior further shows that training more on the local dataset before communicating and updating the model yields better results in the short run but prevents the model from learning and achieving its full potential in the long run. In other words, the model gives too much confidence to its own learning, which is contrary to the case when a round of local training is done on mini-batches and sharing the model is faster. Then, a vast number of iterations are needed to get the desired performance. Hence, both RWM and D-PSGD algorithms update their model based on neighboring ones many times, making the performance difference between them small.
Due to each node having 4 neighbors, the amount of average data shared by a node is four times larger in D-PSGD communication than in RWM. This can be confirmed in Figure \ref{ex1-ml-data} for MovieLens dataset.
\\

% EPOCHS
% Test loss
% RWM (390): 0.76303765340174
% DPSGD (495): 0.668382125391298; (390): 0.687472911606612
% Train loss
% RWM (390): 0.0817472077546819 -- starts overfitting
% DPSGD (497): 0.098516349321555; (390): 0.106317233281547

% STEPS

Next, the experiment of full model sharing is run with CelebA dataset, and the test accuracy for RWM and D-PSGD algorithms is presented in Figure \ref{ex1-celeba-test-acc}. The D-PSGD model gives better results, giving final accuracy of 89.42\% after 250 iterations. The variation of local model performances on nodes is more extensive with RWM, as the model update is done based on a randomly chosen neighbor and with less information than in D-PSGD. For the RWM, the final accuracy after 200 iterations is 86.35\%, while for D-PSGD, the corresponding value is 87.85\%. The situation with calculated losses during training and testing is similar, with D-PSGD giving slightly better results. As the CNN model for CelebA is the smallest, the data overhead in each iteration is smaller than with matrix factorization used with MovieLens, though the pattern remains the same, i.e., the total data shared per node is around 4 times larger using D-PSGD than using RWM.
\\

% STEPS
% Test acc
% RWM (110): 82.5491175083164
% DPSGD (169): 86.3861197219483; (110): 83.4689540181297

% Test loss -- smanjuju se oba i dalje
% RWM (110): 0.400862382470358
% DPSGD (169): 0.321348416145026; (110): 0.386664342390196
% Train loss - isto se smanjuju oba i dalje, dpsd bolji

\begin{figure}[!htbp]
  \centering
  % TEST LOSS MOVIELENS EPOCHS VS STEPS
    \begin{minipage}[b]{0.49\textwidth}
    \includegraphics[width=\textwidth]{plots/ex1-ml-e-test-loss.png}
    \caption{MovieLens: Test loss when training on the whole dataset.}
    \label{ex1-ml-e-test-loss}
\hfill
  \end{minipage}
    \begin{minipage}[b]{0.49\textwidth}
    \includegraphics[width=\textwidth]{plots/ex1-ml-s-test_loss.png}
    \caption{MovieLens: Test loss when training on mini-batches.}
    \label{ex1-ml-s-test_loss}
  \end{minipage}

  \vfill
  % MOVIELENS SHARED DATA
   \begin{minipage}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{plots/ex1-ml-data.png}
    \caption{Average amount of data shared per node in D-PSGD and RWM (in MBs).}
    \label{ex1-ml-data}
  \end{minipage}
    \hfill
    % TEST ACC CELEBA STEPS
    \begin{minipage}[b]{0.49\textwidth}
    \includegraphics[width=\textwidth]{plots/ex1-celeba.png}
    \caption{CelebA: Test accuracy when training on mini-batches and sharing full model.}
    \label{ex1-celeba-test-acc}
  \end{minipage}

\end{figure}

\subsection{Partial Model}
Experiments done with different configurations for partial model sharing showed that the best performance is achieved when aggregation of model parameter changes is done both for local training changes and changes inducted by the model averaging step. Hence, for these experiments, parameters \emph{accumulaiton} and \emph{accumulate\_averaging\_changes} are set to \emph{True}. In the following, observations done on MovieLens and CelebA are presented, with \emph{MovieLens(1)} and \emph{CelebA} configuration parameters defined in Table \ref{table-exp}.
\\

Testing the MovieLens dataset with partial model sharing for both RWM and D-PSGD is done with different values of $\alpha$, such as 0.1, 0.3, and 0.5. In both communication techniques, there is a clear pattern showing how the model performance and fraction of model shared are correlated; the less of a model is shared, the better the performance. This behavior is displayed in Figures \ref{ex2-ml-rwm} and \ref{ex2-ml-dpsgd} that show the model loss on evaluation, where we also present full sharing of the model, i.e., $\alpha=1.0$, which verifies the previous statement. Furthermore, we compare the best models of both communication methods, those where 10\% of the model parameters that changed the most are shared in each round, and plot the testing loss in Figure \ref{ex2-ml-0.1}. It can be observed that RWM is learning more in the beginning until D-PSGD overtakes it around \(140^{th}\) iteration. The D-PSGD continues to be better, and divergence becomes slightly more significant. The test loss for RWM in its final iteration (438) is 0.7460, while for D-PSGD, it is 0.6586 at that moment, leading to a final loss of 0.6243 in the \(500^{th}\) iteration.
\\

% -- MOVIELENS
% Test loss 
% RWM (438): 0.745958829357879
% D-PSGD (438): 0.658569619215207; (498): 0.624337315122531
% Train loss 
% RWM (439): 0.14935003360678
% D-PSGD (439): 0.213530531594713; (498): 00.189309073453467
Next, analyzing the system's total processing time to complete the two runs leads us to the same conclusions. The total time taken for the run with RWM is 7.25 minutes, while D-PSGD takes 8.87 minutes. This difference is due to D-PSGD having more interactions between the nodes, plus each node averages its own model with multiple others, which also increases the computing time. If we observe the performance of D-PSGD given the same time as RWM used, then RWM gives worse results even though it completes more communication rounds. In Figure \ref{ex2-ml-time}, we plot test loss comparison between D-PSGD and RWM given the same processing time, i.e., 7.25 minutes. For this time, the D-PSGD completes 384 iterations yielding a test loss of 0.6969, while that loss for RWM is 0.7460. Therefore, if processing time is taken into account, even though RWM is a faster algorithm, it is clear that D-PSGD produces a better model.
\\

% MovieLens EX2
% D - 8.87 minutes, 500 iter, zavrsi 385
% R - 7.25 minutes, svi 438, 392 iter

For our classification dataset, CelebA, the differences in model performance for various values of $\alpha$ are almost negligible, in contrast to bigger discrepancies observed for MovieLens. However, both for RWM and D-PSGD, the strongest learning and best accuracy are achieved when sharing 30\% of model parameters. This sharing also achieves better performance compared to sharing the full model. The comparison of accuracy on the global testing dataset for both communication techniques is shown in Figure \ref{ex2-cel-acc}. Here, the distinction between RWM and D-PSGD is more prominent for model accuracy, as well as losses during training and testing. In \(127^{th}\) iteration the RWM terminates with 84.01\% model accuracy, while D-PSGD has 86.76\% after completing 127 iterations and 88.87\% in \(170^{th}\) upon its termination. This pattern is also reflected in training loss, which takes values of 0.3506 and 0.2542 upon the termination of RWM and D-PSGD, respectively.
\\

Again, in Figure \ref{ex2-cel-time}, the time processing trade-off is depicted by showing test accuracy achieved when limiting execution time. Since D-PSGD consistently gives better results even when giving the same processing time to both algorithms when running this experiment, its shorter running time does not change the conclusion comparisons. The total time for RWM and D-PSGD to finish initial runs was 147.8 minutes and 194.2 minutes, respectively. 
% Celeba EX2
% D 23:04:10 - 02:18:22 - iter 170 = 194.2 minutes, zavrsi 128 itearacija
% R 22:41:34 - 01:09:22, svi 127 iter 107 = 147.8 minutes

% -- CELEBA
% Test acc
% RWM (127): 84.0134329059227
% DPSGD (127): 86.7571685219107; (169): 88.8747005777089
% - divergence bigger and bigger, also in train and test loss
% Test loss
% RWM (127): 0.368629201670133
% DPSGD (127): 0.31380859434527; (169): 0.267921630154541
% Train loss
% RWM (127): 0.350605749845657
% DPSGD (127): 0.302964139325457; (169): 0.254182000295048

\begin{figure}[!htbp]
  \centering
  
  % TEST LOSS MOVIELENS STEPS - RWM
    \begin{minipage}[b]{0.49\textwidth}
    \includegraphics[width=\textwidth]{plots/ex2-ml-rwm.png}
    \caption{MovieLens: Test loss for partial sharing with RWM.}
    \label{ex2-ml-rwm}
  \end{minipage}
  \hfill
    % TEST LOSS MOVIELENS STEPS - DPSGD
 \begin{minipage}[b]{0.49\textwidth}
    \includegraphics[width=\textwidth]{plots/ex2-ml-dpsgd.png}
    \caption{MovieLens: Test loss for partial sharing with D-PSGD.}
    \label{ex2-ml-dpsgd}
  \end{minipage}
  
    \vfill
    % TEST LOSS MOVIELENS STEPS - COMP
 \begin{minipage}[b]{0.49\textwidth}
    \includegraphics[width=\textwidth]{plots/ex2-ml-0.1.png}
    \caption{MovieLens: Test loss comparison between RWM and D-PSGD for $\alpha=0.1$.}
    \label{ex2-ml-0.1}
  \end{minipage}
    \hfill
    % TEST LOSS MOVIELENS STEPS - TIME
 \begin{minipage}[b]{0.49\textwidth}
    \includegraphics[width=\textwidth]{plots/ex2-ml-time.png}
    \caption{MovieLens: Test loss comparison given the same processing time.}
    \label{ex2-ml-time}
  \end{minipage}

    \vfill
    % TEST ACC CELEBA STEPS - 0.3
 \begin{minipage}[b]{0.49\textwidth}
    \includegraphics[width=\textwidth]{plots/ex2-cel-acc.png}
    \caption{CelebA: Test accuracy comparison between RWM and D-PSGD for $\alpha=0.3$.}
    \label{ex2-cel-acc}
  \end{minipage}
    \hfill
    % TEST ACC CELEBA STEPS - TIME
 \begin{minipage}[b]{0.49\textwidth}
    \includegraphics[width=\textwidth]{plots/ex2-cel-time.png}
    \caption{CelebA: Test accuracy comparison given the same processing time.}
    \label{ex2-cel-time}
  \end{minipage}
\end{figure}

\subsection{Random Alpha Model}
This experiment further expands partial model sharing by randomly choosing a fraction of the model to share from $[0.1, 0.2, 0.3, 0.4, 1.0]$ in each round, with setting \emph{metadata\_cap} to 0.5. The choice of exact model parameters stays the same, meaning that the ones that changed the most, both locally and after averaging, are selected.
\\

For the MovieLens dataset, the experiment was run with 5000 iterations with the testing loss during the time depicted in Figure \ref{ex3-ml-s}. The differences between RWM and D-PSGD are minimal, again with already seen behavior. Only at the start, the random walk model is slightly better, while soon that changes leading to a final loss of 0.3503 in \(4650^{th}\) iteration. For D-PSGD, that value is 0.3234, while it finishes with 0.3189. The evaluation done on the training dataset shows that D-PSGD consistently gives slightly higher errors than RWM, from which it can be concluded that RWM focuses extensively on its local dataset and does not generalize as well as D-PSGD. The training loss values in iteration 4650 are 0.0215  for RWM and 0.0244 for D-PSGD, which then terminates with 0.0229.
\\
% ML
% Test loss
% RWM (4650): 0.350285213952082
% D 0.323423326875755; k 0.318903465930316
% train
% 0.0215130555404487
% 0.0243924866827995; 0.0229226310223976

In experiments done with CelebA dataset and CNN model, the D-PSGD model consistently performs better, with its advantage periodically changing over time. This divergence is shown with obtained accuracy on the testing set for both techniques presented in Figure \ref{ex3-cel-acc}. In iteration 195 RWM achieves 87.17\% accuracy, D-PSGD 89.38\% with increase to 90.84\% in its \(250^{th}\) iteration. For errors on local testing, the situation remains consistent, D-PSGD has better results, and the models slowly diverge more and more.

% Cel - bolji je d sve vreme
% acc
% Rwm (195): 87.1650623870923
% d 90.839145648396 ; (250) 89.3762329153163
% train loss
% r 0.285477736478754
% d 0.242182751190829; 0.211926191882876
\begin{figure}[!htbp]
  \centering
    % TEST LOSS MOVIELENS RANDOM
 \begin{minipage}[b]{0.49\textwidth}
    \includegraphics[width=\textwidth]{plots/ex3-ml-s.png}
    \caption{MovieLens: Test loss comparison between RWM and D-PSGD for random $\alpha$ sharing.}
    \label{ex3-ml-s}
  \end{minipage}
    \hfill
    % TEST ACC CELEBA RANDOM
 \begin{minipage}[b]{0.49\textwidth}
    \includegraphics[width=\textwidth]{plots/ex3-cel-acc.png}
    \caption{CelebA: Test accuracy comparison between RWM and D-PSGD for random $\alpha$ sharing.}
    \label{ex3-cel-acc}
  \end{minipage}

\end{figure}

\pagebreak
\subsection{Sub-Sampling Model}
In this experiment, we examine the sub-sampling sharing on all three datasets configured in Table \ref{table-exp}, performing local training on mini-batches. The choices of $\alpha$ also stay the same; those are 0.1, 0.3, and 0.5. For all datasets, the model performance with both RWM and D-PSGD algorithms is correlated to the fraction of model sharing. The differences are not that extensive, but the models give the best results when sending 50\% of the model, slightly worse when sending 30\%, and the poorest with 10\% sharing. This behavior can be explained by the fact that sub-sampling makes a randomized choice of which parameters to share, unlike partial sharing, which takes the ones that have changed the most since the last time they were shared. Furthermore, sub-sampling performs worse than sending an entire model in each round.  
\\

The behavior is the same for the recommender dataset MovieLens when sharing the whole model, except that performance is worse. The RWM initially learns more in the first iterations, but D-PSGD improves a bit later and stays better. The test loss in \(390^{th}\) iteration is 1.0501 for RWM and 1.0002 for D-PSGD, where the latter model terminates after 500 iterations with 0.8878 loss. Similarly, the CelebA dataset used for classification gives a clearer pattern of $\alpha=0.5$ performing the best, again with identical behavior as with its full model sharing, only worst performance.
\\

% -- MOVIELENS
% pattern the same as with full sharing, graphs etc. random je prvo bolji do 150e iteracije, onda gori
% Test loss
% RWM (390): 1.05006865064353
% DPSGD (390): 1.00016444415539; (498): 0.887795563106491

% CELEBA
% RWM
% - ista situacija, 0.5 najbolji ali full i dalje bolji od toga, clear pattern

The experiments for FEMNIST dataset show similar patterns as the previous two described. The differences between various models are small, both for RWM and D-PSGD. In Figure \ref{ex4-fem-acc-r} we plot test accuracy for RWM, while Figure \ref{ex4-fem-acc-d} represents that for D-PSGD. Both communication techniques give slightly better results when randomly sharing 50\% of the model parameters. Where in Figure \ref{ex4-fem-acc-0.5}, those best results are compared after the system executes 300 iterations. Here, the accuracy does not have an obvious pattern, as RWM performs slightly better in the first iterations, while after, the D-PSGD takes its place. The accuracy for terminating iterations of RWM and D-PSGD is 77.98\% and 81.03\%, respectively.
\\

The time taken for RWM to complete its run was 144.8 minutes, while D-PSGD took 197.08 minutes in total. In Figure \ref{ex4-fem-time}, we plot previously described results taking the time execution of RWM. This does not say much, as the results are similar to before, meaning that the time execution here does not impact much, and there is no trade-off. On the contrary, there is an important trade-off regarding the network load and model accuracy. As D-PSGD has much more communication and model exchanges in the network, in Figure \ref{ex4-fem-data-shared}, we show that load by comparing it to RWM on the average amount of data shared per node. If we estimated the total network load in both systems runs and limited them to use the same amount of data, we would obtain results depicted in Figure \ref{ex4-fem-data}. It can be observed that there is a significant network load trade-off with regards to model accuracy, as the D-PSGD would achieve accuracy slightly bigger than 46.85\%, while RWM would be much better with acquiring 77.98\%.

% Femnist EX4
% D - 197.08 minutes, 300 ukupno, zavrsi 219
% R - 144.8 minutes, svi 230, 0 je 266

% FEMNIST
% Acc
% RWM (230) 77.9803115822687
% D-PSGD (230) 78.9629378127948; (300) 81.0267839297421

\begin{figure}[!htbp]
  \centering
  
  % TEST ACC FEMNIST - RWM
    \begin{minipage}[b]{0.49\textwidth}
    \includegraphics[width=\textwidth]{plots/ex4-fem-acc-r.png}
    \caption{FEMNIST: Test accuracy for sub-sampling sharing with RWM.}
    \label{ex4-fem-acc-r}
  \end{minipage}
  \hfill
    % TEST ACC FEMNIST - DPSGD
 \begin{minipage}[b]{0.49\textwidth}
    \includegraphics[width=\textwidth]{plots/ex4-fem-acc-d.png}
    \caption{FEMNIST: Test accuracy for sub-sampling sharing with D-PSGD.}
    \label{ex4-fem-acc-d}
  \end{minipage}
  
    \vfill
    % TEST ACC FEMNIST - 0.5
 \begin{minipage}[b]{0.49\textwidth}
    \includegraphics[width=\textwidth]{plots/ex4-fem-acc.png}
    \caption{FEMNIST: Test accuracy comparison between RWM and D-PSGD for $\alpha=0.5$.}
    \label{ex4-fem-acc-0.5}
  \end{minipage}
      \hfill
    % TEST ACC FEMNIST STEPS - TIME
 \begin{minipage}[b]{0.49\textwidth}
    \includegraphics[width=\textwidth]{plots/ex4-fem-time.png}
    \caption{FEMNIST: Test accuracy comparison given the same processing time.}
    \label{ex4-fem-time}
  \end{minipage}
  
      \vfill
    % data shared
 \begin{minipage}[b]{0.49\textwidth}
    \includegraphics[width=\textwidth]{plots/ex4-data-sharing.png}
    \caption{Average amount of data shared per node in D-PSGD and RWM.}
    \label{ex4-fem-data-shared}
  \end{minipage}
      \hfill
    % network load
 \begin{minipage}[b]{0.49\textwidth}
    \includegraphics[width=\textwidth]{plots/ex4-fem-data.png}
    \caption{FEMNIST: Test accuracy comparison given the same network load.}
    \label{ex4-fem-data}
  \end{minipage}
\end{figure}