\chapter{Background}\label{background}
This chapter describes the two machine learning models used in the system: a matrix factorization model that falls into the category of recommendation models and a convolutional neural network that we use for category of classification models. Furthermore, decentralized learning is explained.

\section{Matrix Factorization}\label{back-mf}
The first machine learning model, matrix factorization, is used in a recommendation system to predict the "rating" or "preference" a user would give to an item. It can be seen in various use cases, from suggesting buyers items that could interest them to suggesting users text, image, and video content that matches their preferences. It belongs to a class of collaborative methods that extract similarities and make future predictions based on past interactions between users and items (user-item interaction matrix).
\\

The matrix factorization method only relies on user-item interaction information and assumes a latent model to explain these interactions. It decomposes the user-item interaction matrix $A \in \mathbb{R}^{n \times m}$ into a product of two matrices of lower dimension $X \in \mathbb{R}^{n \times k}$ and $Y \in \mathbb{R}^{m \times k}$ representing embeddings that summarise user tastes and items profiles respectively. The embeddings are learned such that the product $XY^T$ is a good approximation of the feedback matrix $A$. The $(i, j)$ entry of $XY^T$ is simply the dot product $<X_i, Y_j>$ of the embeddings of $user_i$ and $item_j$. To make correct predictions, this entry needs to be close to $A_{i,j}$. Difficulties arise as matrix $A$ is often very sparse, with only some user-item interactions being known. An additional regularization parameter $\lambda$ can be added to improve the model performance and avoid overfitting. Moreover, much of the variation in interaction data are due to effects associated with either users or items, known as biases, independent of any interactions. The intuition behind this is that some users give lower or higher ratings than others, and some items receive lower or higher ratings than others systematically. Hence, the bias vectors $b \in \mathbb{R}^{n \times 1}$ and $c \in \mathbb{R}^{m \times 1}$ are included.
Formally, including all terms, the loss function which the model minimizes can be written as follows:
\begin{align*}
&\hphantom{nn-nn-nn-nn}J(X, Y, b, c) = \\
&\frac{1}{2} \sum_{(i, j) \in I} (a_{ij} - b_i - c_j - \sum_{l = 1}^{k} x_{il}y_{jl})^2 + \frac{\lambda}{2} ||X||^2 + \frac{\lambda}{2} ||Y||^2
\end{align*}
where $I$ represents the set of indices for known values in $A$. Upon learning matrix $X$ and matrix $Y$, the predictions for $user_i$ and $item_j$ are obtained as:
\[
p_{ij} = X_i \cdot Y_j + b_i + c_j
\]

\section{CNN (Convolutional Neural Network)}\label{back-cnn}
Convolutional Neural Networks (CNN) are deep learning algorithms that can take in an input image, assign the importance of various aspects and objects, and differentiate them based on the image's learnable weights and biases. Compared to other classification algorithms, CNN requires much less pre-processing. Besides images, they can be used for video, speech, audio signal inputs, etc. Biological processes inspired convolutional networks in that the pattern of connections between neurons resembles that observed in the animal visual cortex. The brain's cortex responds to visual stimuli only in an area called the receptive field, where individual neurons reside. The receptive fields of different neurons partially overlap such that they cover the entire visual area.
\\

The architecture of CNN consists of 3 main types of layers. The first type of layer is a convolutional layer, a core layer, which requires input data, filer, and feature map. It applies a convolution operation to the input, passing the result to the next layer by converting all the pixels in its receptive field into a single value. Then, a pooling layer represents downsampling and performs dimensionality reduction. Similarly to a convolutional layer, it applies a filter across the entire input but without weights and reduces the number of input parameters. The final layer is the fully connected (FC) layer, where each node in the output layer connects directly to a node in the previous layer. The FC layer performs classification with features extracted through the previous layers and their filters. An example of CNN model architecture that performs the classification of a handwritten digit is shown in Figure \ref{cnn}. The CNN becomes more complex with each layer, whereas earlier layers focus on simple features such as colors and edges. As the image data progresses through the CNN layers, the model begins to recognize more prominent elements of the image until it finally identifies the object.
\\
\\

\begin{figure}[!htbp]
  \centering
  \begin{minipage}[b]{0.9\textwidth}
    \includegraphics[width=\textwidth]{imgs/cnn.jpeg}
    \caption{CNN model sequence to classify handwritten digits.~\cite{cnn}}
    \label{cnn}
  \end{minipage}
\end{figure}

\section{Decentralized Learning}\label{back-dl}
In a complex ecosystem of servers where data are inherently distributed and can be sensitive or high-volume, decentralized machine learning techniques are most helpful. They enable collaborative model training without sharing raw data and can be used to combine all local learnings from inherently distributed datasets into a single machine learning model. These algorithms are called gossip algorithms, in which each device only communicates with its neighbors without requiring an aggregation server or any central component.
\\

For instance, in a recommender system, the number of items and users grows by the minute; hence one of the main challenges of centralized recommenders remains their scalability. Therefore, decentralized systems can be proposed for recommendation purposes. Typically, we assume that nodes are connected according to a specific network topology in such a system. After performing the local learning task, each node forwards some information to its neighbors. For example, this information can be the output of the local learning tasks in decentralized learning systems or users' profiles. Relying on such a gossiping protocol enables the data or model to be disseminated in the network until convergence is reached. Training data is fully distributed to the nodes in the system, i.e., raw data is produced by users who hold a single personal profile record. Such approaches let the users' data stay where it is produced, thus limiting their exposure. Nevertheless, models need to be aggregated in order to provide relevant recommendations for unseen items. Learning tasks are performed on local data on user devices and merged in the decentralized learning system through a gossip-based protocol. Model aggregation and exchanging information between neighbors can have various approaches and implementations based on the system's needs.
