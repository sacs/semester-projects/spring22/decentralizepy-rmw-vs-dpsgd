\chapter{Implementation}\label{implementation}
This section first describes the architecture of the system, followed by different communication methods between the nodes in the network and model averaging. Next, the four methods for sharing model parameters are presented. Lastly, in Section \ref{config}, we present the system configuration for running specific datasets, the machine learning model, its optimization and parameters tuning, as well as which communication and sharing technique are to be used.

\section{System Architecture}
The system consists of multiple components that encapsulated produce a configurable decentralized system. It supports different network sizes and node interconnections, the dataset and machine learning model used, various model sharing, and neighboring communication schemes.
\\

At the start of the run, the nodes are initialized, and each holds the data of several users. Each node's initially distributed local data points are disjunct, and nodes perform training only on their local data set. The received dataset is split into a train set, used to improve the model, and a test set to check whether the model outputs accurate values compared to the ground truth. These two datasets are created by taking a corresponding ratio from each user's data samples. Then, each node can start epochs of training locally and share information with other nodes depending on the model sharing method and communication technique specified. This training step typically consists of passing data through the machine learning model, calculating the gradient, back-propagation, and the optimizer step. After each training step on local data, a node waits or sends model information. Depending on the communication technique used, a node updates its local model with model parameters received from other neighbors. Finally, the system continuously provides evaluation metrics of each local model, which later enables us to process the final performance as an average of all nodes.

\section{Communication}\label{comm}
For communication inside the nodes network, we can use one of the two algorithms that differ in the number of neighbors they exchange model with and correspondingly model updates. The first algorithm is D-PSGD, where each node communicates and exchanges information with all its neighbors in each training epoch. A node receives model parameters from all its neighbors, and weights attributed to each model are calculated using the Metropolis-Hastings protocol. On the other hand, the RWM algorithm randomly chooses a neighbor it sends its model to and continues local training only upon receiving a message from a neighbor.

\subsection{D-PSGD}
Decentralized SGD (Stochastic Gradient Descent) is the application of solving the optimization problem of finding minimum loss function by updating parameters one by one for each data point in a decentralized setting. In the Decentralized Parallel Stochastic Gradient Descent (D-PSGD) algorithm, nodes average their local model with those of all its neighbors after a step of training on local data. Then, each node sends its models and an integer corresponding to its degree (number of neighbors) to all neighbors. Upon receiving a model, it is merged with its own through a weighted average. For that, we use Metropolis-Hastings weights, where each model parameter $p_k$ in a node $k$ is computed by attributing weights to the contributions of its $N$ neighbors and that of itself, $p^{t+1}_k=w_k p^{t}_k+\sum_{i=1}^{N}w_i p_i^t$, with $w_i=\frac{1}{1+max(d_k, d_i)}$ and $w_k=1-\sum_{i=1}^{N}w_i$, where $d_i$ is the degree of node $i$. If a node has not received given model parameter, we consider only those of its neighbors, $w_k=0$, and the weight attributed to each neighbor, just like Metropolis-Hastings, is inversely proportional to their degree, i.e., $w_i=\frac{1}{1+d_i\sum_{j=1,i \neq j}^{N}\frac{1}{d_j}}$.

\subsection{RWM (Random Walk Model)}
The second algorithm resembles the idea of random walks, a random process that describes a path that consists of a succession of random steps. Here, the communication choices of nodes are independent and random. In the Random Walk Model algorithm, nodes average their local model upon receiving a model from one neighbor after a step of training on local data. Hence, the communication is asynchronous as a node progresses to the next round only after receiving a model. The model update is done by weighted averaging the received model with the local one. The weight of a model corresponds to confidence in the trained model, represented as the model's age. This age depends on the number of local training steps a node completes and the age of the nodes from which it receives the model. The age is increased at each local training step and additionally updated to the age of the received neighbor model if it is older, i.e., its age is bigger. After model averaging, a node sends its updated model and an integer corresponding to its age to one neighbor selected uniformly at random.
\\

Due to the asynchronous advancement of nodes to further rounds, the system terminates once the first node completes the intended number of iterations. However, when calculating the performance metrics of an obtained run, we use the data from rounds contained in all nodes, i.e., we take the minimum number of iterations reached by all nodes.

\section{Sharing Methods}\label{sharing}
The system provides multiple choices of algorithms used for sharing information about the local model with other nodes. It is important to note that only the model parameters are shared. We use a couple of approaches to experiment between sharing the whole model, or only a fraction of it, as well as picking the suitable parameters to share. 

\subsection{Full Model}
The \emph{Full Model} sharing approach represents the most simple idea of sharing model parameters; the whole model is shared. Here, when a node needs to share its information, it sends all of the model parameters. The sharing configuration for this is the following:
\begin{lstlisting}[language=Python]
sharing_package = decentralizepy.sharing.Sharing
sharing_class = Sharing
\end{lstlisting}

\subsection{Partial Model}
The second sharing method resembles sending only a fraction of the whole model. Nodes have specified float number $\alpha$ representing a fraction of the model parameters to share. Furthermore, picking which ones to send can be further adjusted by defining what changes in the model should be accumulated. In every communication round $\alpha$ fraction of the parameters that changed the most since the last time they were shared is chosen. Using \emph{accumulation} means that the accumulation of local training changes measures changes. Additionally, using \emph{accumulate\_averaging\_changes} adds to the accumulation the changes made due to the model averaging step. These accumulation choices can be specified in the configuration depicted below by setting corresponding values to \emph{True} or \emph{False}. The model changes can not be accumulated only based on model averaging; hence if any accumulation is to be done, the local changes must be included. In other words, setting \emph{accumulate\_averaging\_changes} to \emph{True} and \emph{accumulation} to \emph{False} has no effect. Also, in configuration, $\alpha$ is set to a float number between 0 or 1, with one meaning the full model should be shared (equivalent to \emph{Full Model} sharing).

\begin{lstlisting}[language=Python]
sharing_package = decentralizepy.sharing.PartialModel
sharing_class = PartialModel
alpha = 0.3
accumulation = True
accumulate_averaging_changes = True
\end{lstlisting}

\subsection{Random Alpha Model}
This sharing method extends the previously presented \emph{Partial Model} by allowing multiple possible fractions of a model to be shared. In each round of sharing, a random value representing the model fraction is chosen from a list of possible fractions. This list is called \emph{alpha\_list}, and it contains float numbers between 0 and 1, representing possible $\alpha$ used in the run. The configuration for this sharing method is presented below, with \emph{metadata\_cap} defining the threshold for sharing the entire model, i.e., if the randomly chosen $\alpha$ from the list is bigger than \emph{metadata\_cap}, then the full model is shared.

\begin{lstlisting}[language=Python]
sharing_package = decentralizepy.sharing.RandomAlpha
sharing_class = RandomAlpha
alpha_list = [0.1, 0.2, 0.3, 0.4, 1.0]
metadata_cap = 0.5
accumulation = True
accumulate_averaging_changes = True
\end{lstlisting}

\subsection{Sub-Sampling Model}
Sub-sampling is a method that reduces data size by selecting a subset of the original data. Hence, the sub-sampling sharing is a simple version of partial sharing where the subset of model parameters to share is chosen randomly without inspecting any model parameter changes. This method uses defined $\alpha$ to create a random binary mask utilized to determine the $\alpha$ fraction of model parameters to be shared. The configuration to use this sharing method is the following:

\begin{lstlisting}[language=Python]
sharing_package = decentralizepy.sharing.SubSampling
sharing_class = SubSampling
alpha = 0.3
\end{lstlisting}

\section{System Configuration}\label{config}
The system can be applied to different networks, datasets, machine learning algorithms, communication techniques, and sharing methods. All these choices are defined in more detail as configuration parameters of the system. Each configuration setup yields one unique experiment, and the results metrics are stored for each node of the system on the machine executing that process. To execute a run of the system, the following parameters are specified:
\begin{itemize}
	\item \textbf{DATASET:} Specifies the dataset used for training and evaluation, the location of train and test data, as well as the class implementing the machine learning model. An example configuration is as follows:
	\begin{lstlisting}[language=Python]
    dataset_package = decentralizepy.datasets.MovieLens
    dataset_class = MovieLens
    model_class = MatrixFactorization
    train_dir = /mnt/nfs/shared/leaf/data/movielens
    test_dir = /mnt/nfs/shared/leaf/data/movielens
    \end{lstlisting}
    
	\item \textbf{OPTIMIZER PARAMETERS:} Defines which machine learning optimizer is used and the learning rate of the model.
	\begin{lstlisting}[language=Python]
    optimizer_package = torch.optim
    optimizer_class = SGD
    lr = 0.25
    \end{lstlisting}
    
	\item \textbf{TRAIN PARAMETERS:} The configuration of the training module for each node. It specifies \emph{rounds} representing the number of iterations per training step, a Boolean parameter \emph{full\_epochs} indicating whether training is done on full dataset (\emph{True}) or mini-batches (\emph{False}), and \emph{batch\_size} defining the number of samples in one batch of machine learning model step. Also, the loss function of the machine learning model is defined here.
	\begin{lstlisting}[language=Python]
    training_package = decentralizepy.training.Training
    training_class = Training
    rounds = 9
    full_epochs = False
    batch_size = 8
    shuffle = True
    loss_package = torch.nn
    loss_class = MSELoss
    \end{lstlisting}

	\item \textbf{COMMUNICATION:} Defines the implementation of network communication API with IP addresses of used machines. The addresses are provided as a path to the JSON file containing the mapping of the machine id to its IP address.
	\begin{lstlisting}[language=Python]
    comm_package = decentralizepy.communication.TCP
    comm_class = TCP
    addresses_filepath = /home/dresevic/decentralizepy/ip_4Machines.json
    \end{lstlisting}
    
	\item \textbf{SHARING:} The configuration of the sharing method used, described in Section \ref{sharing}, with additional parameter \emph{random\_sharing} indicating whether a model is shared only with one random neighbor or all neighbors. In other words, setting \emph{random\_sharing = True} means that communication uses \emph{Random Walk Model}, otherwise \emph{D-PSGD}.
\end{itemize}